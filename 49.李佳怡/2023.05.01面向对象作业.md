将以下描述信息转换为java代码：

1、定义员工Employee类,该类具有如下成员:

(1) 属性：姓名(name,字符串类型)，工号(workId,字符串类型)，部门(dept,字符串类型),属性私有
(2) 方法:
		1. 空参数构造和满参数构造
		2. 提供属性的set/get方法
		3. 定义showMsg方法抽象方法
2、定义经理Manager类继承Employee类，该类具有如下成员:

(1) 属性:职员Clerk(该经理的职员)
(2) 方法:
		1. 空参数构造方法和满参数构造方法
		2. 属性的get和set方法
		3. 重写父类的showMsg方法，按照要求实现信息打印
3、定义职员Clerk类继承Employee类，该类具有如下成员:

(1) 属性:经理Manager(该职员的经理)
(2) 方法:
		1. 空参数构造方法和满参数构造方法
		2. 属性的get和set方法
		3. 重写父类的showMsg方法，按照要求实现信息打印
4、创建Test测试类，测试类中创建main方法，main方法中创建经理对象和职员对象，信息分别如下:

经理：工号为 ""M001"",名字为 张小强，部门为 销售部
职员：工号为 C001,名字为 李小亮，部门为 销售部 

经理的职员为李小亮，职员的经理为张小强
分别调用经理的showMsg方法和职员的showMsg方法打印结果：

销售部的：张小强，员工编号：M001
他的职员是李小亮
销售部的：李小亮，员工编号：C001
他的经理是张小强
public abstract class Employee {
    private String name;
    private String workid;
    private String dept;

    public Employee() {
    }

    public Employee(String name, String workid, String dept) {
        this.name = name;
        this.workid = workid;
        this.dept = dept;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWorkid() {
        return workid;
    }

    public void setWorkid(String workid) {
        this.workid = workid;
    }

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }
    public abstract void showMsg();
}
class Manager extends Employee{
    String Clerk;

    public Manager() {
        super();
    }

    public Manager(String workid, String name, String dept,String clerk) {
        super(name, workid, dept);
        Clerk = clerk;
    }

    public String getClerk() {
        return Clerk;
    }


    public void setClerk(String clerk) {
        Clerk = clerk;
    }

    @Override
    public void showMsg() {
        System.out.println("销售部的："+ getName()+",员工编号："+getWorkid()+"\n" +
                "他的职员是"+Clerk);

    }
}
class Clerk extends Employee{
    String Manager;

    public Clerk( String workid,String name, String dept,String manager) {
        super(name, workid, dept);
        Manager=manager;
    }
    public void setName(String name){

    }
    public Clerk() {
    }

    public String getManager() {
        return Manager;
    }
    public String getname(){
        return super.getName();
    }

    public void setManager(String manager) {
        Manager = manager;
    }


    @Override
    public void showMsg() {
            System.out.println("销售部的："+ getName()+",员工编号："+getWorkid()+"\n" +
                    "他的经理是"+Manager);

    }
}
class text{
    public static void main(String[] args) {
        new Clerk("c001","李小亮","销售部","张小强").showMsg();
        new Manager("M001", "张小强", "销售部","李小亮").showMsg();


    }
}