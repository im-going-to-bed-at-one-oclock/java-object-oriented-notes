# 第5章 面向对象基础（上） 

## 实例变量练习题

### 1、圆类

（1）声明一个圆的图形类，包含实例变量/属性：半

（2）在测试类的main中，创建2个圆类的对象，并给两个圆对象的半径属性赋值，最后显示两个圆的半径值、周长和面积值

​	提示：圆周率可以使用Math.PI

```java
public class A2 {
    public static void main(String[] args) {
        A1 a1 = new A1(23);
        A1 a2 = new A1(14);
        a1.area();
        a2.area();
    }
}
```

```java
public class A1 {
    private int r;
    
    public int getR() {
        return r;
    }
    
    public void setR(int r) {
        this.r = r;
    }

    public String toString() {
        return "半径";
    }

    public A1(int r) {
        this.r = r;
    }
    public A1() {
    }

    public void area(){
        System.out.println("半径为"+r+"的圆的面积为"+Math.PI*r*r+"周长为"+Math.PI*r*2);
    }
}
```



### 2、学生类

（1）声明一个学生类，包含实例变量/属性：姓名和成绩

（2）在测试类的main中，创建2个学生类的对象，并给两个学生对象的姓名和成绩赋值，最后输出显示

```JAVA
public class A1 {
    private String name;
    private double cj;

	public String getName() {
    	return name;
	}

	public void setName(String name) {
    	this.name = name;
	}

	public double getCj() {
    	return cj;
	}

	public void setCj(double cj) {
    	this.cj = cj;
	}

	public String toString() {
    	return "stu";
	}

	public A1(String name, double cj) {
    	this.name = name;
    	this.cj = cj;
	}

	public A1() {
	}
	public void a(){
    	System.out.println(name+"的成绩为"+cj);
	}
}
```

```JAVA
public class A2 {
    public static void main(String[] args) {
        A1 a1 = new A1("xm",12);
        A1 a2 = new A1("xh",14);
        a1.a();
        a2.a();
    }
}
```



### 3、日期和员工类

（1）声明一个MyDate类型，有属性：年，月，日

（2）声明另一个Employee类型，有属性：姓名（String类型），生日（MyDate类型）

（3）在测试类中的main中，创建两个员工对象，并为他们的姓名和生日赋值，并显示

```java
public class MyDate {
    private int year;
    private int month;
    private int day;

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public MyDate(int year, int month, int day) {
        this.year = year;
        this.month = month;
        this.day = day;
    }

    public MyDate() {
    }
}
```

```java
public class Employee{
    private String name;
    private MyDate date;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MyDate getDate() {
        return date;
    }

    public void setDate(MyDate date) {
        this.date = date;
    }
}
```

```java
public class Main {
    public static void main(String[] args) {
        Employee emp1 = new Employee();
        Employee emp2 = new Employee();
        emp1.setName("小明");
        emp1.setName("小红");
        emp1.setDate(new MyDate(2013,12,24));
        emp2.setDate(new MyDate(2015,3,2));
        a(emp1);
        a(emp2);

    }
    public static void a(Employee a){
        System.out.println(a.getName()+"是"+a.getDate().getYear()+"年"+a.getDate().getMonth()+"月"+a.getDate().getDay()+"日出生的");
    }
}
```



## 实例方法的声明与调用练习

### 1、圆类改造

（1）声明一个圆的图形类，包含实例变量/属性：半径

（2）将圆求面积、求周长、返回圆对象信息分别封装为3个方法

double area()：求面积

double perimeter()：求周长

String getInfo()：返回圆对象信息，例如："半径：xx，周长：xx，面积：xx"

（3）在测试类的main中，创建2个圆类的对象，并给两个圆对象的半径属性赋值，最后显示两个圆的半径值、周长和面积值

​	提示：圆周率可以使用Math.PI

```java
public class A1 {
    private int r;

    public int getR() {
        return r;
    }

    public void setR(int r) {
        this.r = r;
    }

    public String toString() {
        return "半径";
    }

    public A1(int r) {
        this.r = r;
    }
    public A1() {
    }

    public String getInfo(){
        return  "半径为"+r+"的圆的面积为"+area()+"周长为"+perimeter();
    }
    public double perimeter(){
        return Math.PI*r*2;
    }
    public double area(){
        return Math.PI*r*r;
    }
}
```

```java
public class A2 {
    public static void main(String[] args) {
        A1 a1 = new A1(23);
        A1 a2 = new A1(14);
        System.out.println(a1.getInfo());
        System.out.println(a2.getInfo());
    }
}
```

### 2、日期和员工类改造

（1）声明一个MyDate类型

- 有属性：年，月，日

- 增加一个String getDateInfo()方法，用于返回日期对象信息，例如：xx年xx月xx日

（2）声明另一个Employee类型，

- 有属性：姓名（String类型），生日（MyDate类型）

- 增加一个void setBirthday(int year, int month, int day)方法，用于给员工生日赋值
- 增加一个String getEmpInfo()方法，用于返回员工对象信息，例如：姓名：xx，生日：xx年xx月xx日

（3）在测试类中的main中，创建两个员工对象，并为他们的姓名和生日赋值，并显示

```java
public class MyDate {
    private int year;
    private int month;
    private int day;

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public MyDate(int year, int month, int day) {
        this.year = year;
        this.month = month;
        this.day = day;
    }

    public MyDate() {
    }

    public String getDateInfo(){
        return year+"年"+month+"月"+day+"日";
    }
}
```

```java
public class Employee{
    private String name;
    private MyDate date;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MyDate getDate() {
        return date;
    }

    public void setDate(MyDate date) {
        this.date = date;
    }
    public  String getEmpInfo(){
       return "姓名:"+name+"生日"+date.getDateInfo();
    }
    public void setBirthday(int year, int month, int day){
        this.date=new MyDate(year,month,day);
    }
}
```

```java
public class Main {
    public static void main(String[] args) {
        Employee emp1 = new Employee();
        Employee emp2 = new Employee();
        emp1.setName("小明");
        emp2.setName("小红");
        emp1.setBirthday(2013,12,23);
        emp2.setBirthday(2011,3,13);
        System.out.println(emp1.getEmpInfo());
        System.out.println(emp2.getEmpInfo());
    }
}
```



### 3、MyInt类

（1）声明一个MyInt类，

- 包含一个int类型的value属性
- 包含一个方法boolean isNatural()方法，用于判断value属性值是否是自然数。自然数是大于等于0的整数。
- 包含一个方法int approximateNumberCount()方法，用于返回value属性值的约数个数。在[1, value]之间可以把value整除的整数都是value的约数。
- 包含一个方法boolean isPrimeNumber()方法，用于判断value属性值是否是素数。如果value值在[1, value]之间只有1和value本身两个约数，并且value是大于1的自然数，那么value就是素数。
- 包含一个方法int[] getAllPrimeNumber()方法，用于返回value属性值的所有约数。返回[1, value]之间可以把value整除的所有整数。

（2）测试类的main中调用测试

```java
public class MyInt {
    private int Value;

    public int getValue() {
        return Value;
    }

    public void setValue(int value) {
        Value = value;
    }

    public boolean isNatural(){
        boolean a = true;
        if (Value<0){
            a=false;
        }
        return a;
    }
    public int approximateNumberCount(){
        int count=0;
        for (int i = 1; i < Value; i++) {
            if(Value % i==0){
                count++;
            }
        }
        return count;
    }
    public boolean isPrimeNumber(){
        boolean a = true;
        for (int i = 2; i < Value; i++) {
            if(Value % i == 0){
                a=false;
            }
        }
        return a;
    }

    public int[] getAllPrimeNumber(){
        int[] a = new int[0];
        int count=0;
        for (int i = 1; i <= Value; i++) {
            if(Value % i==0){
                a= Arrays.copyOf(a,a.length+1);
                a[count]=i;
                count++;
            }
        }
        return a;
    }
}
```

```java
public class Main {
    public static void main(String[] args) {
        MyInt my1 = new MyInt();
        my1.setValue(41);
        System.out.println(my1.isNatural());
        System.out.println(my1.approximateNumberCount());
        System.out.println(my1.isPrimeNumber());
        System.out.println(Arrays.toString(my1.getAllPrimeNumber()));
    }
}
```

## 参数练习

### 1、n个整数中的最小值和n个整数的最大公约数

（1）声明方法int min(int... nums)	：返回n个整数中的最小值

（2）声明方法int maxApproximate(int... nums)：返回n个整数的最大公约数

```java
public class M {
    public static void main(String[] args) {
        A a = new A();
        int[] arr = {32,42,544,36,434};
        a.setA(arr);
        System.out.println(a.maxApproximate());
        System.out.println(a.min());
    }
}
```

```java
public class A {
    private int[] a;

    public int[] getA() {
        return a;
    }

    public void setA(int[] a) {
        this.a = a;
    }

    public int min() {
        int min = a[0];
        for (int j : a) {
            if (min > j) {
                min = j;
            }
        }
        return min;
    }

    public int maxApproximate() {
        int[] b = a;
        Arrays.sort(b);
        int min = 1;
        for (int i = 0; i < b.length-1; i++) {
            for (int j = 1; j <= b[i]; j++) {
                if (b[i] % j == 0 && b[i+1] % j == 0) {
                    min=j;
                }
            }
        }
        return min;
    }
}
```

### 2、判断程序运行结果

```java
public class Tools{
	public static void main(String[] args) {
		int i = 0;
		new Tools().change(i);
		i = i++;
		System.out.println("i = " + i);
	}

	void change(int i){
		i++;
	}
}
```

### 3、数组长度扩大2倍

（1）声明数组工具类ArraysTools

- 方法1：String toString(int[] arr)，遍历结果形式：[元素1，元素2，。。。]

- 方法2：int[] grow(int[] arr)，可以实现将一个数组扩大为原来的2倍


（2）在测试类的main中调用测试

```java
import java.util.Arrays;

public class Arr {
    public int[] arr(int[] arr){
        int[] b = new int[arr.length*2];
        for (int i = 0; i < arr.length; i++) {
            b[i]=arr[i];
        }
        return b;
    }
    public String[] arr(String[] arr){
        String[] b = new String[arr.length*2];
        for (int i = 0; i < arr.length; i++) {
            b[i]=arr[i];
        }
        return b;
    }
    public double[] arr(double[] arr){
        double[] b = new double[arr.length*2];
        for (int i = 0; i < arr.length; i++) {
            b[i]=arr[i];
        }
        return b;
    }
    public float[] arr(float[] arr){
        float[] b = new float[arr.length*2];
        for (int i = 0; i < arr.length; i++) {
            b[i]=arr[i];
        }
        return b;
    }
    public char[] arr(char[] arr){
        char[] b = new char[arr.length*2];
        for (int i = 0; i < arr.length; i++) {
            b[i]=arr[i];
        }
        return b;
    }
}

```

```java
import java.util.Arrays;

public class M {
    public static void main(String[] args) {
        int[] arr = {1,3,4,13,3,7};
        Arr arr1 = new Arr();
        System.out.println(Arrays.toString(arr));
        arr=arr1.arr(arr);
        System.out.println(Arrays.toString(arr));
    }
}

```

## 方法重载练习

### 1、比较两个数大小关系

（1）声明MathTools工具类，包含：

- int compare(int a, int b)：比较两个整数大小关系，如果第一个整数比第二个整数大，则返回正整数，如果第一个整数比第二个整数小，则返回负整数，如果两个整数相等则返回0；
- int compare(double a, double b)：比较两个小数大小关系，如果第一个小数比第二个小数大，则返回正整数，如果第一个小数比第二个小数小，则返回负整数，如果两个小数相等则返回0；
- int compare(char a, char b)：比较两个字符大小关系，如果第一个字符比第二个字符编码值大，则返回正整数，如果第一个字符比第二个字符编码值小，则返回负整数，如果两个字符相等则返回0；

（2）在测试类的main方法中调用

```java
import java.util.Arrays;

public class MathTools {
    public int compare(int a, int b){
        if(a>b)return -1;
        else if (a<b) return -1;
        else return 0;
    }
    public int compare(double a, double b){
        if(a>b)return -1;
        else if (a<b) return -1;
        else return 0;
    }
    public int compare(char a, char b){
        if(a>b)return -1;
        else if (a<b) return -1;
        else return 0;
    }
}

```

```java
public class M {
    public static void main(String[] args) {
        MathTools mathTools = new MathTools();
        System.out.println(mathTools.compare(3,4));
    }
}
```



### 2、数组排序和遍历

（1）声明一个数组工具类ArraysTools，包含几个重载方法

- 重载方法系列1：可以为int[]，double[]，char[]数组实现从小到大排序
  - void sort(int[] arr)
  - void sort(double[] arr)
  - void sort(char[] arr)

- 重载方法系列2：toString方法，可以遍历int[]，double[]，char[]数组，遍历结果形式：[元素1，元素2，。。。]
  - String toString(int[] arr)
  - String toString(double[] arr)
  - String toString(char[] arr)

（2）在测试类的main方法中调用

```java
import java.util.Arrays;

public class ArraysTools {
    public void sort(int[] arr){
        Arrays.sort(arr);
    }
    public void sort(double[] arr){
        Arrays.sort(arr);
    }
    public void sort(char[] arr){
        Arrays.sort(arr);
    }
    public String toString(int[] arr){
        return Arrays.toString(arr);
    }
    public String toString(double[] arr){
        return Arrays.toString(arr);
    }
    public String toString(char[] arr){
        return Arrays.toString(arr);
    }
}
```

```java
import java.util.Arrays;

public class M {
    public static void main(String[] args) {
        int[] arr = {123,123,123,123,32,42};
        ArraysTools arraysTools = new ArraysTools();
        arraysTools.sort(arr);
        System.out.println(arraysTools.toString(arr));
    }
}

```



### 3、求三角形面积

（1）声明一个图形工具类GraphicTools，包含两个重载方法

- 方法1：double triangleArea(double base ,double height)，根据底边和高，求三角形面积，
- 方法2：double triangleArea(double a,double b,double c)，根据三条边，求三角形面积，根据三角形三边求面积的海伦公式： 

![1577091140580](第5章 面向对象基础（上）.assets/1577091140580.png)

提示：Math.sqrt(x)，表示求x的平方根

（2）在测试类的main方法中调用

```java
import java.util.Arrays;

public class M {
    public static void main(String[] args) {
        GraphicTools graphicTools = new GraphicTools();
        System.out.println(graphicTools.triangleArea(3, 2, 2));
        System.out.println(graphicTools.triangleArea(3, 2));
    }
}

```

```java
import java.util.Arrays;

public class GraphicTools {
    public double triangleArea(double base ,double height){
        return base*height/2;
    }
    public double triangleArea(double a,double b,double c){
        double p = (a+b+c)/2;
        return Math.sqrt(p*(p-a)*(p-b)*(p-c));
    }
}
```

## 方法的递归调用练习

### 1、猴子吃桃

猴子吃桃子问题，猴子第一天摘下若干个桃子，当即吃了所有桃子的一半，还不过瘾，又多吃了一个。第二天又将仅剩下的桃子吃掉了一半，又多吃了一个。以后每天都吃了前一天剩下的一半多一个。到第十天，只剩下一个桃子。试求第一天共摘了多少桃子？

![1573725022751](第5章 面向对象基础（上）.assets/1573725022751.png)

```java
import java.util.Arrays;

public class M {
    public static void main(String[] args) {
        int count = 0;
        while (true){
            count++;
            double a=count;
            for (int i = 1; i < 11; i++) {
                a=a/2-1;
            }
            if(a==1){
                System.out.println(count/2+1);
                break;
            }
        }
    }
}

```



### 2、走台阶

有n级台阶，一次只能上1步或2步，共有多少种走法？

![1573724181996](第5章 面向对象基础（上）.assets/1573724181996.png)

```java
import java.util.Arrays;

public class M {
    public static void main(String[] args) {
        n(2);
    }
    public static void n(int n){
        int count = 0 ;
        int a = 0 ;
        for (int i = 0; i <= n; i++) {
            a=i;
            for (int j = a; j <= n; j+=2) {
                if(a==n){
                    count++;
                }
                a+=2;
            }
        }
        System.out.println(count);
    }
}

```

### 3、求n!

![1573725058457](第5章 面向对象基础（上）.assets/1573725058457.png)

```java

```

## 对象数组练习

### 1、学生对象数组

（1）定义学生类Student

- 声明姓名和成绩实例变量，

- String getInfo()方法：用于返回学生对象的信息


（2）测试类的main中创建一个可以装3个学生对象的数组，从键盘输入3个学生对象的信息，并且按照学生成绩排序，显示学生信息

```java
import java.util.Arrays;

public class Student {
    @Override
    public String toString() {
        return "Student{" +
                "name=" + name +
                ", cj=" + cj +
                '}';
    }

    public Student() {
    }

    public Student(String name, int cj) {
        this.name = name;
        this.cj = cj;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCj() {
        return cj;
    }

    public void setCj(int cj) {
        this.cj = cj;
    }

    private String name;
    private int cj;

    public String getInfo(){
        return name+"考了"+cj+"分";
    }
}
```

```java
import java.util.Arrays;
import java.util.Scanner;

public class M {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Student student1 = new Student(sc.next(), sc.nextInt());
        Student student2 = new Student(sc.next(), sc.nextInt());
        Student student3 = new Student(sc.next(), sc.nextInt());
    }
}
```

### 2、请使用二维数组存储如下数据，并遍历显示

```java
		String[][] employees = {
		        {"10", "1", "段誉", "22", "3000"},
		        {"13", "2", "令狐冲", "32", "18000", "15000", "2000"},
		        {"11", "3", "任我行", "23", "7000"},
		        {"11", "4", "张三丰", "24", "7300"},
		        {"12", "5", "周芷若", "28", "10000", "5000"},
		        {"11", "6", "赵敏", "22", "6800"},
		        {"12", "7", "张无忌", "29", "10800","5200"},
		        {"13", "8", "韦小宝", "30", "19800", "15000", "2500"},
		        {"12", "9", "杨过", "26", "9800", "5500"},
		        {"11", "10", "小龙女", "21", "6600"},
		        {"11", "11", "郭靖", "25", "7100"},
		        {"12", "12", "黄蓉", "27", "9600", "4800"}
		    };
```

其中"10"代表普通职员，"11"代表程序员，"12"代表设计师，"13"代表架构师

![1561529559251](第5章 面向对象基础（上）.assets/1561529559251.png)

```java
import java.util.Arrays;

public class M {
    public static void main(String[] args) {
        String[][] employees = {
                {"10", "1", "段誉", "22", "3000"},
                {"13", "2", "令狐冲", "32", "18000", "15000", "2000"},
                {"11", "3", "任我行", "23", "7000"},
                {"11", "4", "张三丰", "24", "7300"},
                {"12", "5", "周芷若", "28", "10000", "5000"},
                {"11", "6", "赵敏", "22", "6800"},
                {"12", "7", "张无忌", "29", "10800","5200"},
                {"13", "8", "韦小宝", "30", "19800", "15000", "2500"},
                {"12", "9", "杨过", "26", "9800", "5500"},
                {"11", "10", "小龙女", "21", "6600"},
                {"11", "11", "郭靖", "25", "7100"},
                {"12", "12", "黄蓉", "27", "9600", "4800"}
        };
        for (int i = 0; i < employees.length; i++) {
            for (int j = 0; j < employees[i].length; j++) {
                System.out.print(employees[i][j]+" ");
            }
            System.out.println();
        }
    }

}
```

### 3、杨辉三角

* 使用二维数组打印一个 10 行杨辉三角.

  1

  1 1

  1 2 1

  1 3 3  1

  1 4 6  4  1

  1 5 10 10 5 1

   ....

* 开发提示

1. 第一行有 1 个元素, 第 n 行有 n 个元素

2. 每一行的第一个元素和最后一个元素都是 1

3. 从第三行开始, 对于非第一个元素和最后一个元素的元素. 

   ```
   yanghui[i][j] = yanghui[i-1][j-1] + yanghui[i-1][j];
   ```

   ![1558397196775](第5章 面向对象基础（上）.assets/1558397196775.png)
   
   ```java
   import java.util.Arrays;
   
   public class M {
       public static void main(String[] args) {
           int[][] a = new int[11][11];
           a[1][1]=1;
           for (int i = 2; i < 11; i++) {
               for (int j = 1; j < i; j++) {
                   a[i][j]=a[i-1][j]+a[i-1][j-1];
               }
           }
           for (int i = 2; i < 11; i++) {
               for (int j = 1; j < i; j++){
                   System.out.print(a[i][j]+"\t");
               }
               System.out.println();
           }
       }
   }
   
   ```



